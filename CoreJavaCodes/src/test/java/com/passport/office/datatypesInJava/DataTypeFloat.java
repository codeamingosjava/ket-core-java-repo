package com.passport.office.datatypesInJava;

/**
 * Created by dotcom on 10/28/17.
 */
public class DataTypeFloat {

    public static void main(String[] args) {
        float a = 23f;
        float a1 = 345216f;
        // 32-bit single precision number

        float a2 = 2344f;
        //        default value for integer is zero
        float a3 = 0f;
        System.out.println(a3);
    }

}
