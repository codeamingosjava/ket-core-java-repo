package com.passport.office.datatypesInJava;

/**
 * Created by dotcom on 10/28/17.
 */
public class DataTypeInteger {

    public static void main(String[] args) {
        Integer a = 23;
        int a1 = 34;
        //32-bit
        //-2,147,483,648 through 2,147,483,647
        int a2 = 2344;
//        default value for integer is zero
        int a3 = 0;
        System.out.println(a3);
    }
}
