package com.passport.office.datatypesInJava;

/**
 * Created by dotcom on 10/28/17.
 */
public class DataTypeLong {

    public static void main(String[] args) {
        long a = 23L;
        long a1 = 345216l;
        // 64-bit
        //- 9,223,372,036,854,775,808 through  9,223,372,036,854,775,807
        long a2 = 2344l;
        //        default value for integer is zero
        long a3 = 0L;
        System.out.println(a3);
    }
}
