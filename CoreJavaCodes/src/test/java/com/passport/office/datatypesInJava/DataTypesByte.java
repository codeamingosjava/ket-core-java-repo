package com.passport.office.datatypesInJava;

/**
 * Created by dotcom on 10/28/17.
 */
public class DataTypesByte {

/* Multiline comments
    public static void main(String[] args) {
        byte a = 127;
        byte a1 = 0;

        //a byte is 8 bits //Single line comment
        //Byte max number is 127
        //Byte range is -128 through 127 2p7 -1
        System.out.println(a1);


    }

    */


    public static void main(String[] args) {
        //I will revert back late to sort this out
        byte a = 127;
        byte a1 = 0;
        byte a2 = -70;

        //a byte is 8 bits
        //Byte max number is 127
        //Byte range is -128 through 127 2p7 -1
        System.out.println(a1);


    }
}
