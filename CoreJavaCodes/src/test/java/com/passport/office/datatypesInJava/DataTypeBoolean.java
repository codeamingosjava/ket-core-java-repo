package com.passport.office.datatypesInJava;

/**
 * Created by dotcom on 10/28/17.
 */
public class DataTypeBoolean {

    public static void main(String[] args) {
        boolean doesItRainToday = true;
        Boolean areHavingClassToday = false;

        System.out.println(doesItRainToday);
        System.out.println(areHavingClassToday);
    }
}
