package com.passport.office.datatypesInJava;

/**
 * Created by dotcom on 10/28/17.
 */
public class DataTypeDouble {

    public static void main(String[] args) {
        double a = 2334.098d;
        double a1 = 345216.9454d;
        // 64-bit double precision

        double a2 = 2344d;
        //        default value for integer is zero
        double a3 = 0d;
        System.out.println(a3);
    }

}
