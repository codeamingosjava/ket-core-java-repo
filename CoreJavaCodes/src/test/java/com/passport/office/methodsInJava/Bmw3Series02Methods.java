package com.passport.office.methodsInJava;

/**
 * Created by dotcom on 11/11/17.
 */
public class Bmw3Series02Methods {

    /*
        Methods are action word in Java programming
        A method has to either start with a verb or is just
        There are kinds of a method
            1. Smart Method - Method that can do a job but does report back to you and does ask you what to do
            2. Smarter Method - Method that also doesn't report you but ask you what needs to be done
            3. Smartest Method - Methods that report back and also ask what needs to be done
     */

    //Smart method
    public void calculateBmwAge(){
        int yearOfReg = 2011;
        int age = 2017 - yearOfReg;
        System.out.println(age);
    }

    //Smarter method
    public void calculateBmwAge(int yearOfReg){
        int age = 2017 - yearOfReg;
        System.out.println(age);
    }


    //Smartest method
    public double calculateBmwAge(double yearOfReg){
        double age = 2017 - yearOfReg;
        return age;
    }

    public double calculateBmwAge1(){
        double age = 2017 - 2011;
        return age;
    }


    /* Types of methods
        1. Instance/Object Methods
        2. Static/Class methods
     */

    public static double checkMot(){
        double age = 2017 - 2011;
        return age;
    }

}
