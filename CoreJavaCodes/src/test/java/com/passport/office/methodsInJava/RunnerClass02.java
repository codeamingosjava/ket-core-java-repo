package com.passport.office.methodsInJava;

import com.passport.office.classesInJava.Orange;

/**
 * Created by dotcom on 11/11/17.
 */
public class RunnerClass02 {

    public static void main(String[] args) {
        Bmw3Series02Methods bmw = new Bmw3Series02Methods();

        //Instance.methodName
        bmw.calculateBmwAge();

        //ClassName.methodName
        Bmw3Series02Methods.checkMot();

        Orange.getOrangeName();


        Orange myOrange = new Orange();
        myOrange.getOrangePrice();
        myOrange = null;
        myOrange.getOrangePrice();
    }
}
