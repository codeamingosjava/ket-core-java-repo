package com.passport.office.decisionStatements;

/**
 * Created by dotcom on 10/31/17.
 */
public class JavaDecisionThree {

    public static void main(String[] args) {
        double num1 = 4d;

        if(num1 == 1d){
            System.out.println("BMW");
        }else if(num1 ==2d){
            System.out.println("Merc");
        }else if (num1 == 3d){
            System.out.println("VW");
        }else{
            System.out.println("Mustang");
        }
    }
}
