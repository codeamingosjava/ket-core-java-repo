package com.passport.office.decisionStatements;

/**
 * Created by dotcom on 10/31/17.
 */
public class JavaDecisionOne {

    public static void main(String[] args) {
        String tomorrow = "Wednesday";

        if(tomorrow.equalsIgnoreCase("Wednesday")){
            System.out.println("I am going to the office!");
        }

    }
}
