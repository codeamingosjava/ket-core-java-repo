package com.passport.office.decisionStatements;

/**
 * Created by dotcom on 10/31/17.
 */
public class JavaDecisionFive {

    public static void main(String[] args) {

        int num1 = 1;

        switch (num1){
            case 1:
            case 4:
                System.out.println("BMW");
                break;
            case 2:
                System.out.println("Merc");
                break;
            case 3:
                System.out.println("VW");
                break;
            default:
                System.out.println("Mustang");
                break;
        }

    }
}
