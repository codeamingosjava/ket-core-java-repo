package com.passport.office.decisionStatements;



/**
 * Created by dotcom on 10/31/17.
 */
public class JavaDecisionFour {


    //Nested statement in Java
    public static void main(String[] args) {
        char grade = 'A';
        int score = 70;

        if(grade == 'A'){
            System.out.println("You made a distinction");
            if(score >= 90 && score <95){
                System.out.println("You have scholarship to student in UNN");
            }else if(score >=96){
                System.out.println("You have scholarship to student in Oxford");
            }else{
                System.out.println("But unforunately you haven't secured scholarship");
            }
        }else if(grade == 'B'){
            System.out.println("YOu made a merit");
        }else if(grade == 'C'){
            System.out.println("YOu made a Credit");
        }
    }
}
