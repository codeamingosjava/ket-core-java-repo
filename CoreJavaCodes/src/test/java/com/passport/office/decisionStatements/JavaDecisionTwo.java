package com.passport.office.decisionStatements;

/**
 * Created by dotcom on 10/31/17.
 */
public class JavaDecisionTwo {

    public static void main(String[] args) {
        int num = 4;

        if(num == 2){
            System.out.println("The number is definitely two");
        }else{
            System.out.println("I am not sure of the number but it is not two");
        }
    }
}
