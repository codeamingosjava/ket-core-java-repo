package com.passport.office.basics_oop.v2;

/**
 * Created by dotcom on 11/28/17.
 */
public class BarclaysAccount {

    public double accountBalance = 10000d;


    public void showAccountBalance(){
        this.shout();
        System.out.println(accountBalance);
    }


    protected void shout(){
        System.out.println("What's my name?");
    }
}
