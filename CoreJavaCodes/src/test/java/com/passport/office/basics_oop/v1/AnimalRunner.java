package com.passport.office.basics_oop.v1;

/**
 * Created by dotcom on 11/28/17.
 */
public class AnimalRunner {

    public static void main(String[] args) {
        Animal animal = new Animal();

//        animal.animalName = "Green Horse";

        animal.showAnimalName();
    }
}
