package com.passport.office.basics_oop.v2;

/**
 * Created by dotcom on 11/28/17.
 */
public class AccountRunner {

    public static void main(String[] args) {
        BarclaysAccount account = new BarclaysAccount();

//        account.accountBalance = 0d;

        account.showAccountBalance();

    }
}
