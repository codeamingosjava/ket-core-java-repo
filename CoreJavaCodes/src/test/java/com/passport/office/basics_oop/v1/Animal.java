package com.passport.office.basics_oop.v1;

/**
 * Created by dotcom on 11/28/17.
 */
public class Animal {

    public String animalName = "Green Eagle";
    public int animalAge;
    public double animalSpeed;
    public long animalHeight;
    public boolean hasTail;


    public void showAnimalName(){
        System.out.println(animalName);
    }


    public String getAnimalName(){
        return animalName;
    }




}
