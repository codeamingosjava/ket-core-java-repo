package com.passport.office.variablesinJava;

/**
 * Created by dotcom on 11/11/17.
 */
public class Car {

    /*
        There three kinds of variables
        1. Instance Variables = Variable that lives outside of a method, can have access modifiers
        2. Static/Class variable = Variable that lives outside of a method but also has static in it and can have access modifiers
        3. Local Variables  = Variable that lives inside of a method but must not have access modifiers
        4. Constants = This is used to permanently store values
     */

    public String carName = "BMW 3 Series 2011";
    public static int carAge = 6;
    private static final int CAR_MILEAGE = 2000;


    public String getRegNumber1(){
        //Local variable
        String regNumber1 = "BK61VHL";
        return regNumber1 +carAge;
    }

    public static String getRegNumber2(){
        //Local variable
        String regNumber2 = "HK61BKT";
        return regNumber2 +carAge ;
    }


}
