package com.passport.office.variablesinJava;

/**
 * Created by dotcom on 11/11/17.
 */
public class VariablesRunner {

    public static void main(String[] args) {
        Car car = new Car();
        car.carName = "BMW 5 Series";

        car.getRegNumber1();


        //STATIC Variable usage
        Car.carAge = 7;
        Car.getRegNumber2();


    }
}
