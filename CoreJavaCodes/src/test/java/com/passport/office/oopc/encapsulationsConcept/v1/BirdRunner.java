package com.passport.office.oopc.encapsulationsConcept.v1;

/**
 * Created by dotcom on 12/2/17.
 */
public class BirdRunner {

    public static void main(String[] args) {
        Bird bird = new Bird();

        bird.setBirdName("Dove"); // To change the value
        String name1 = bird.getBirdName(); //To get the value
        bird.setBirdName("Elephant");
        String name2 = bird.getBirdName();


        bird.showBirdName();
    }
}
