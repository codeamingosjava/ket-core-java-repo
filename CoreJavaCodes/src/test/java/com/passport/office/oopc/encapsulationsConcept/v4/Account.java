package com.passport.office.oopc.encapsulationsConcept.v4;

/**
 * Created by dotcom on 12/2/17.
 */
public class Account {


    private double accountBalance;

    public Account(double accountBalance) {
        this.accountBalance = accountBalance;
    }


    public double getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(int pin, double accountBalance) {
        if(pin == 2222){
            System.out.println("Access granted");
            if(accountBalance >= this.accountBalance){
                this.accountBalance = this.accountBalance + accountBalance;
            }
        }else{
            System.out.println("Access denied");
        }

    }
}
