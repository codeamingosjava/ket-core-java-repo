package com.passport.office.oopc.encapsulationsConcept.v1;

/**
 * Created by dotcom on 12/2/17.
 */

public class Bird {


    private String birdName = "Kite";
    private int noOfLegs;
    private double speedOfFlight;


    public String getBirdName() {
        return birdName;
    }

    public void setBirdName(String birdName) {
        this.birdName = birdName;
    }

    public int getNoOfLegs() {
        return noOfLegs;
    }

    public void setNoOfLegs(int noOfLegs) {
        this.noOfLegs = noOfLegs;
    }

    public double getSpeedOfFlight() {
        return speedOfFlight;
    }

    public void setSpeedOfFlight(double speedOfFlight) {
        this.speedOfFlight = speedOfFlight;
    }

    public void showBirdName(){
        double speed = 23d;
        System.out.println("This bird name is "+ birdName + " and the speed of flight is "+ speed);
    }



}
