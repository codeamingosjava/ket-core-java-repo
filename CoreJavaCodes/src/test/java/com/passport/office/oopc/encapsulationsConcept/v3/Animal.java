package com.passport.office.oopc.encapsulationsConcept.v3;

/**
 * Created by dotcom on 12/2/17.
 */
public class Animal {

    private String name;
    private int age;
    private boolean hasEaten;

    public Animal(String name, int age, boolean hasEaten){
        this.name = name;
        this.age = age;
        this.hasEaten = hasEaten;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        if(name != null){
            this.name = name;
        }else{

        }

    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isHasEaten() {
        return hasEaten;
    }

    public void setHasEaten(boolean hasEaten) {
        this.hasEaten = hasEaten;
    }
}
