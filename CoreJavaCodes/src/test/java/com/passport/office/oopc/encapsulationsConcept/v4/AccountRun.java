package com.passport.office.oopc.encapsulationsConcept.v4;

/**
 * Created by dotcom on 12/2/17.
 */
public class AccountRun {

    public static void main(String[] args) {
        Account barclaysAtm = new Account(50d);

        barclaysAtm.setAccountBalance(2222, 1000);
    }
}
