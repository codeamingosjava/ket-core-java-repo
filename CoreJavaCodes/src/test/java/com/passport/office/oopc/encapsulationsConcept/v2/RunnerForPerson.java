package com.passport.office.oopc.encapsulationsConcept.v2;

/**
 * Created by dotcom on 12/2/17.
 */
public class RunnerForPerson {


    public static void main(String[] args) {
        //Instantiate a class
        Person adam = new Person();

        //Set all your data
        adam.setFirstName("Adam");
        adam.setLastName("Eden");
        adam.setAge(32);
        adam.setGender("Male");
        adam.setMarried(true);

        //Consume your data
        int age1 = adam.getAge();
        String fName1 = adam.getFirstName();
        String lName1 = adam.getLastName();
        String gender1 = adam.getGender();
        boolean marriedStatus1 = adam.isMarried();

        //Second code for a woman
        Person eve = new Person();

        //Set all your data
        eve.setFirstName("Even");
        eve.setLastName("Fruit");
        eve.setAge(30);
        eve.setGender("Female");
        eve.setMarried(true);

        //Consume your data
        int age2 = eve.getAge();
        String fName2 = eve.getFirstName();
        String lName2 = eve.getLastName();
        String gender2 = eve.getGender();
        boolean marriedStatus2 = eve.isMarried();
    }
}
