package com.passport.office.dataTypeConversion;

/**
 * Created by dotcom on 10/31/17.
 */
public class ConverterNumbersToString {


    public static void main(String[] args) {


        int num1 = 23;
        int num2 = 20;

        String numone = Integer.toString(num1);
        String numTwo = Integer.toString(num2);

        String result = numone + numTwo;

        System.out.println(result);
    }
}
