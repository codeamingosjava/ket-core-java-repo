package com.passport.office.dataTypeConversion;

import java.util.Scanner;

/**
 * Created by dotcom on 10/31/17.
 */
public class ConverterStringToNumbers {


    public static void main(String[] args) {

        //Convert String to Double
        Scanner scan = new Scanner(System.in);

        System.out.println("Enter your first number");
        String t1 = scan.nextLine();
        System.out.println("Enter your second number");
        String t2 = scan.nextLine();

        double t3  = Double.parseDouble(t1);
        double t4  = Double.parseDouble(t2);


        System.out.println(t3 + t4);

        //Convert String to Integer
        String num1 = "23";
        String num2 = "20";

        int numone = Integer.parseInt(num1);
        int numTwo = Integer.parseInt(num2);

        int result = numone + numTwo;

        System.out.println(result);





    }
}
