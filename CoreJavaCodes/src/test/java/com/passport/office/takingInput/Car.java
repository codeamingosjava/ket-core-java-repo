package com.passport.office.takingInput;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by dotcom on 10/28/17.
 */
public class Car {


    public static void main(String[] args) throws IOException {

        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);

        System.out.println("Enter your name");
        String value = br.readLine();

        String greet = "Good morning Mr. "+ value;

        System.out.println(greet);
    }
}
