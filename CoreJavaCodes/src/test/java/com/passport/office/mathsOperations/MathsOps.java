package com.passport.office.mathsOperations;

/**
 * Created by dotcom on 10/31/17.
 */
public class MathsOps {

    public static void main(String[] args) {
        /*  Addition
            Multiplication
            Division
            Subtraction
            Modulus
         */

        int a = 8;
        int b = 3;

        //Add
        System.out.println(a + b);

        //Multiply
        System.out.println(a * b);

        //Divide
        System.out.println(a / b);

        //Subtract
        System.out.println(a - b);


        //Modulus
        System.out.println(a % b);
    }
}
