package uk.co.itv.introOop;

/**
 * Created by dotcom on 11/7/17.
 */
public class BMW {

    // Variables
    int noOftryes = 4;
    String colours = "Red";
    boolean hasHeadlight = true;
    double fuelLitres = 63.00d;
    float chasiNumber = 716156f;

    public void move(){
        System.out.println("Yes I am a BMW and I can move");
        System.out.println("Yes I am a BMW and I can move");
        System.out.println("Yes I am a BMW and I can move");
    }

    public void run(){
        System.out.println("Yes I am a BMW and I can run");
        System.out.println("Yes I am a BMW and I can run");
        System.out.println("Yes I am a BMW and I can run");
        System.out.println("Yes I am a BMW and I can run");
        System.out.println("Yes I am a BMW and I can run");
    }

    public void drive(){
        System.out.println("Yes I am a BMW and I can drive");
        System.out.println("Yes I am a BMW and I can drive");
        System.out.println("Yes I am a BMW and I can drive");
        System.out.println("Yes I am a BMW and I can drive");
    }

    public void turn(){
        System.out.println("This is turn");
    }

}
