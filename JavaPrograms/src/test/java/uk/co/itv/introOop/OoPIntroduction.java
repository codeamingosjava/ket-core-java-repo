package uk.co.itv.introOop;

/**
 * Created by dotcom on 11/7/17.
 */
public class OoPIntroduction {

    /*
        Object Oriented Programming components are
        Classes, Variables, Methods & Objects
     */

    public static void main(String[] args) {

        BMW series3 = new BMW();
        series3.drive();

        BMW series5 = new BMW();
        series5.drive();

    }
}
