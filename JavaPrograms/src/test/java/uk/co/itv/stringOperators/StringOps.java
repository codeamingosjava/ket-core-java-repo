package uk.co.itv.stringOperators;

/**
 * Created by dotcom on 11/7/17.
 */
public class StringOps {

    public static void main(String[] args) {
        String footBall = "FC Barcelona";

        int res10 = footBall.length();
        System.out.println(res10);

        boolean res8 = footBall.equals("FC Madrid");
        System.out.println(res8);

        boolean res9 = footBall.equalsIgnoreCase("FC barcelona");
        System.out.println(res9);



        char res7 = footBall.charAt(3);
        System.out.println(res7);


        String name = "Steve Rich";

        boolean res4 =  name.startsWith("S");
        System.out.println(res4);
        boolean res5 = name.endsWith("h");
        System.out.println(res5);

        boolean res6 = name.contains("Steve");
        System.out.println(res6);



        String statement = " This is the man who has Mercedez Benz ";

        String res3 = statement.trim();
        System.out.println(res3);


        //Convert all of them to lower case
        String res1 = statement.toLowerCase();
        System.out.println(res1);

        //Convert to Upper case
        String res2 = statement.toUpperCase();
        System.out.println(res2);



    }


}
