package uk.co.itv.controlsStatement;

import org.junit.Test;

/**
 * Created by dotcom on 11/7/17.
 */
public class ControlsStatements {

    /*
        While statement
        DoWhile statement
        For statement
        Enhanced For statement

     */


    // While statement
    @Test
    public void testControls1(){
        int x = 15;
        while (x > 1){
            System.out.println("The current value of x is "+x);
            x--;
        }
    }


    //For loop/For Statement
    @Test
    public void testControls2(){
        for(int x=15; x >1; x--){
            System.out.println("The current value of x is "+x);
        }
    }

    //Enhanced For loop/For Statement
    @Test
    public void testControls3(){
        String names[] = new String[]{"Kenneth", "Ade", "Paul", "Ifeanyi"};

        for(String name : names){
            System.out.println(name);
        }
    }


    @Test
    public void testControls4(){
        int x = 15;
        do {
            System.out.println("The current value of x is "+x);
            x--;
        }while (x>1);
    }



}
